export const FORMATS = [
  'NONE',
  'NUMBER',
  'BOOLEAN',
  'DATETIME',
  'CDATA',
  'URI'];
