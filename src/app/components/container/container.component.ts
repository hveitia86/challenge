import {Component, OnInit} from '@angular/core';
import {Categories} from '../../entities/categories';
import {DATATYPES} from '../../entities/dataTypes';
import {FORMATS} from '../../entities/formats';
import {FormControl, FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn} from '@angular/forms';


@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {

  categoriesList = [];
  attributesList: FormGroup;
  dataTypes = DATATYPES;
  formats = FORMATS;
  removable = true;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {

    Categories.forEach(value => {
      this.categoriesList.push({title: value});
    });

    this.attributesList = this.fb.group({
      attributes: this.fb.array([])
    });

  }

  dataTypeChange(index: number) {

    if (this.attributesList.value.attributes[index].dataType === 'OBJECT') {

      this.attributesList['controls']['attributes']['controls'][index]['controls']['defaultValue'].setValue('');
      this.attributesList['controls']['attributes']['controls'][index]['controls']['defaultValue'].disable();

      this.attributesList['controls']['attributes']['controls'][index]['controls']['format'].setValue('NONE');
      this.attributesList['controls']['attributes']['controls'][index]['controls']['format'].disable();

      this.attributesList['controls']['attributes']['controls'][index]['controls']['enumerations'].setValue([]);

      this.attributesList['controls']['attributes']['controls'][index]['controls']['rangeMin'].setValue(null);
      this.attributesList['controls']['attributes']['controls'][index]['controls']['rangeMax'].setValue(null);

    } else {

      this.attributesList['controls']['attributes']['controls'][index]['controls']['defaultValue'].enable();
      this.attributesList['controls']['attributes']['controls'][index]['controls']['format'].enable();

    }
  }

  formatsChange(index: number) {

    if (this.attributesList.value.attributes[index].format !== 'NONE') {
      this.attributesList['controls']['attributes']['controls'][index]['controls']['enumerations'].setValue([]);

      if (this.attributesList.value.attributes[index].format === 'NUMBER') {

        this.attributesList['controls']['attributes']['controls'][index]['controls']['rangeMin'].setValue(0);
        this.attributesList['controls']['attributes']['controls'][index]['controls']['rangeMax'].setValue(1);
        this.attributesList['controls']['attributes']['controls'][index]['controls']['precision'].setValue(null);
        this.attributesList['controls']['attributes']['controls'][index]['controls']['accuracy'].setValue(null);
      }

    } else {
      this.attributesList['controls']['attributes']['controls'][index]['controls']['rangeMin'].setValue(null);
      this.attributesList['controls']['attributes']['controls'][index]['controls']['rangeMax'].setValue(null);
      this.attributesList['controls']['attributes']['controls'][index]['controls']['precision'].setValue(null);
      this.attributesList['controls']['attributes']['controls'][index]['controls']['accuracy'].setValue(null);

    }
  }

  addEnumerationsValue(index: number) {

    const valueToAdd = this.attributesList['controls']['attributes']['controls'][index]['controls']['enumerationValue'].value;

    if (valueToAdd && valueToAdd !== '') {

      this.attributesList['controls']['attributes']['controls'][index]['controls']['enumerations'].value.push(valueToAdd);

      this.attributesList['controls']['attributes']['controls'][index]['controls']['enumerationValue'].setValue('');
    }


  }

  deleteEnumerationValue(indexControl: number, indexEnum: number) {

    this.attributesList['controls']['attributes']['controls'][indexControl]['controls']['enumerations'].value.splice(indexEnum, 1);
  }

  buildItem(category: string) {

    const newForm = new FormGroup({
      category: new FormControl(category),
      name: new FormControl(null, [Validators.required, this.uniqueValueValidator()]),
      description: new FormControl(null, Validators.required),
      deviceResourceType: new FormControl({value: '', disabled: true}),
      defaultValue: new FormControl(null, Validators.required),
      dataType: new FormControl('STRING'),
      format: new FormControl('NONE'),
      enumerations: new FormControl([]),
      rangeMin: new FormControl(null, [Validators.required, this.rangeValidMinValidator()]),
      rangeMax: new FormControl(null, [Validators.required, this.rangeValidMaxValidator()]),
      precision: new FormControl(null, [Validators.required, this.precisionValidator()]),
      accuracy: new FormControl(null, [Validators.required, this.precisionValidator()]),
      unitMeasurement: new FormControl(),
      enumerationValue: new FormControl()
    });


    newForm['controls']['name'].markAsTouched();
    newForm['controls']['description'].markAsTouched();
    newForm['controls']['defaultValue'].markAsTouched();
    newForm['controls']['precision'].markAsTouched();
    newForm['controls']['accuracy'].markAsTouched();

    return newForm;

  }

  uniqueValueValidator(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } => {

      let exist = false;

      this.attributesList.value.attributes.forEach(x => {
        if (control.value === x.name) {
          exist = true;
        }
      });

      return exist ? {'name': {value: control.value}} : null;

    };
  }

  rangeValidMinValidator(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } => {

      let invalid = false;

      if (control.parent && control.value > control.parent.controls['rangeMax'].value) {

        invalid = true;
      }

      return invalid ? {'invalid': {value: 'invalid'}} : null;

    };

  }

  rangeValidMaxValidator(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } => {

      let invalid = false;

      if (control.parent && control.value < control.parent.controls['rangeMin'].value) {
        invalid = true;
      }

      return invalid ? {'invalid': {value: 'invalid'}} : null;

    };

  }

  precisionValidator(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } => {

      let invalid = false;

      if (control.parent) {

        const dif = control.parent.controls['rangeMax'].value - control.parent.controls['rangeMin'].value;

        if (dif % control.value !== 0) {
          invalid = true;
        }

      }

      return invalid ? {'invalid': {value: 'invalid'}} : null;

    };
  }

}


